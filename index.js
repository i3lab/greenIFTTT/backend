const http = require('http');
const express = require("express");
const oasTools = require('@oas-tools/core');
require("dotenv").config();
const jwt = require("jsonwebtoken");
const cors = require("cors")


const serverPort = 8080;
const app = express();
app.use(cors());
app.use(express.json({limit: '50mb'}));

const config = {
    middleware: {
        security: {
            auth: {
                bearerAuth: async (authorization) => {
                    let token = authorization.split(" ")[1];
                    try{
                        let decoded = jwt.verify(token, process.env.TOKEN_KEY);
                    } catch(err) {
                        throw new Error('Invalid Token');
                    }
                }
            }
        }
    }
}

oasTools.initialize(app, config).then(() => {
    http.createServer(app).listen(serverPort, () => {
    console.log("\nApp running at http://localhost:" + serverPort);
    console.log("________________________________________________________________");
    if (!config?.middleware?.swagger?.disable) {
        console.log('API docs (Swagger UI) available on http://localhost:' + serverPort + '/docs');
        console.log("________________________________________________________________");
    }
    });
});
