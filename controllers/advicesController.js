const service = require('../services/advicesService.js');

module.exports.getAdvices = function getAdvices(req, res) {
    service.getAdvices(req, res);
}

module.exports.addAdvice = function addAdvice(req, res) {
    service.addAdvice(req, res);
}

