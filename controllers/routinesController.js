const service = require('../services/routinesService.js');

module.exports.getRoutines = function getRoutines(req, res) {
    service.getRoutines(req, res);
}

module.exports.updateRoutine = function updateRoutine(req, res) {
    service.updateRoutine(req, res);
}

module.exports.addRoutine = function addRoutine(req, res) {
    service.addRoutine(req, res);
}

module.exports.deleteRoutine = function deleteRoutine(req, res) {
    service.deleteRoutine(req, res);
}

