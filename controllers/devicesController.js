const service = require('../services/devicesService.js');

module.exports.getDevices = function getDevices(req, res) {
    service.getDevices(req, res);
}

module.exports.updateDevice = function updateDevice(req, res) {
    service.updateDevice(req, res);
}

module.exports.addDevice = function addDevice(req, res) {
    service.addDevice(req, res);
}

module.exports.deleteDevice = function deleteDevice(req, res) {
    service.deleteDevice(req, res);
}

