const service = require('../services/activitiesService.js');

module.exports.getActivities = function getActivities(req, res) {
    service.getActivities(req, res);
}

module.exports.updateActivity = function updateActivity(req, res) {
    service.updateActivity(req, res);
}

module.exports.addActivity = function addActivity(req, res) {
    service.addActivity(req, res);
}

module.exports.deleteActivity = function deleteActivity(req, res) {
    service.deleteActivity(req, res);
}

