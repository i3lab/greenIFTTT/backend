const service = require('../services/chatService.js');

module.exports.chatGPTCall = function chatGPTCall(req, res) {
    service.chatGPTCall(req, res);
}

module.exports.getChatHistory = function getChatHistory(req, res) {
    service.getChatHistory(req, res);
}

module.exports.deleteChatHistory = function deleteChatHistory(req, res) {
    service.deleteChatHistory(req, res);
}

