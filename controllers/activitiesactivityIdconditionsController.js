const service = require('../services/activitiesactivityIdconditionsService.js');

module.exports.getConditions = function getConditions(req, res) {
    service.getConditions(req, res);
}

module.exports.addCondition = function addCondition(req, res) {
    service.addCondition(req, res);
}

module.exports.updateCondition = function updateCondition(req, res) {
    service.updateCondition(req, res);
}

module.exports.deleteCondition = function deleteCondition(req, res) {
    service.deleteCondition(req, res);
}

