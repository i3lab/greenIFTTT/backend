/**
 * Questo modulo, esterno al backend, è stato creato per simulare il log di dati che vengono inviati sistema, associati ai devices (sensori e dispositivi) inseriti dall'utente.
 */

const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();
const appliancesConsumptions = require('./../frontend/src/assets/deviceTypes.json');

// WebSocket Server for triggering the new state of the devices
const { WebSocketServer } = require('ws');
const wss = new WebSocketServer({ port: 8081 });
const SOCKETS = new Set();

wss.on('connection', function connection(ws, req) {
    ws.on('error', (e) => { console.log("\x1b[95m [WEBSOCKET] Console Error: " + JSON.stringify(e) + " \x1b[0m") });

    ws.on('message', (data) => {
        console.log("\x1b[95m [WEBSOCKET] New client with these Devices: " + data + " \x1b[0m")
        SOCKETS.add({ ws: ws, devices: JSON.parse(data).devices })
    })
});

const SECONDS = 30;


function getConsumptionByValue(value) {
    const appliance = appliancesConsumptions.find(group => group.group === "House Appliances");
    if (appliance) {
        const device = appliance.items.find(item => item.value === value);
        if (device && device.consumption) {
            return device.consumption;
        }
    }
    return null;
}

function confrontaOrario(orario, confrontaCon = "", what="equal") {
    if(confrontaCon === ""){
        let oraCorrente = new Date();
        confrontaCon = `${oraCorrente.getHours().toString().padStart(2, '0')}:${oraCorrente.getMinutes().toString().padStart(2, '0')}`
    }
    
    if (orario === confrontaCon && what === "equal") {
        return true
    } else if (orario < confrontaCon && what === "before") {
        return true
    } else if (orario > confrontaCon && what === "after") {
        return true
    } else { 
        return false 
    }
}

async function countSatisfiedConditions(actorType, deviceId, oldStatus) {
    // let activities = await prisma.activity.findMany({ include: { routine: true }, where: { deviceId: deviceId, status: "active", routine: { status: "active" }}});
    let activities = await prisma.activity.findMany({ where: { deviceId: deviceId }});
    if(activities && activities.length > 0){

        let triggers = [];
    
        for(activity of activities){
            let routine = await prisma.routine.findFirst({ where: { id: activity.routineId }});
            if(routine.status === "active" && activity.status === "active"){
                
                let conditions = await prisma.condition.findMany({ where: { activityId: activity.id }});
            
                let satisfiedCount = 0;
                let allConditionsCount = conditions.length;
                
                for (let condition of conditions) {
                    let satisfied = false;
                    
                    let value = 0.0;
                    if (condition.actorType === 'timestamp') {
                        const oraCorrente = new Date();
                        value = `${oraCorrente.getHours().toString().padStart(2, '0')}:${oraCorrente.getMinutes().toString().padStart(2, '0')}`;
                    } else {
                        value = 0.0;
                        try {
                            value = await prisma.logger.findFirst({ where: { deviceId: deviceId }, orderBy: { timestamp: 'desc' } }).value || 0.0;
                        } catch (err) {
                            value = 0.0;
                        }
                    }
                    
                    switch (condition.operator) {
                        case '=':
                            satisfied = confrontaOrario(value, condition.value1, "equal");
                            break;
                        case '>':
                            satisfied = confrontaOrario(value, condition.value1, "after");
                            break;
                        case '<':
                            satisfied = confrontaOrario(value, condition.value1, "before");
                            break;
                        case '>=':
                            satisfied = (confrontaOrario(value,  condition.value1, "after") || confrontaOrario(value,  condition.value1, "equal"));
                            break;
                        case '<=':
                            satisfied = (confrontaOrario(value,  condition.value1, "before") || confrontaOrario(value,  condition.value1, "equal"));
                            break;
                        case '!=':
                            satisfied = !(confrontaOrario(value, condition.value1, "equal"));
                            break;
                        case 'between':
                            satisfied = (confrontaOrario(value,  condition.value1, "after") && confrontaOrario(value,  condition.value2, "before"));
                            break;
                        case 'not between':
                            satisfied = (confrontaOrario(value,  condition.value1, "before") || confrontaOrario(value,  condition.value2, "after"));
                            break;
                        default:
                            break;
                    }
                    
                    if (satisfied) {
                        satisfiedCount++;
                        console.log("\x1b[32m \t condition '" + condition.name + "' satisfied!\x1b[0m")
                    } else {
                        console.error("\x1b[31m \t condition '" + condition.name + "' NOT satisfied!\x1b[0m")
                    }
                }
    
                console.log("\x1b[36m \t " + satisfiedCount + "/" + allConditionsCount + " conditions satisfied for activity '" + activity.name + "' (" + (activity.andConditions ? 'conditions are in AND' : 'conditions are in OR') + ")\x1b[0m")
    
                if((activity.andConditions && allConditionsCount == satisfiedCount) || (!activity.andConditions && satisfiedCount > 0)){
                    triggers.push(true);
                } else {
                    triggers.push(false);
                }
            }
        }
    
        return triggers.includes(true);
    } else { return oldStatus === 'active'; }
}
  
/*
 * Se il device è un HOME APPLIANCE, posso loggare in value un valore random 
 * tenendo conto del tipo (utilizzo dataset) e triggerando il nuovo stato 
 * se tutte le condizioni sono state raggiunte.
 */

// Ottieni il numero totale di elementi nel database
setInterval(function(){
    new Promise((resolve, reject) => {
        resolve(prisma.device.findMany({
            where: {
                NOT: {
                    type: {
                        // TODO: aggiungere sensori qui!!!
                        in: ["temperature", "humidity", "P1", "P2"],
                    }
                }
            },
        }));
    }).then((devices) => {
        for(let device of devices) {
            // console.log("Device: " + JSON.stringify(device))
            // estraggo un elemento casuale dalla lista dei devices
            // let randomIndex = Math.floor(Math.random() * devices.length);
            // console.log("Random Extraction of a Device (#" + randomIndex + "/#" + devices.length + ")");
            // let device = devices[randomIndex];
            console.log("\x1b[33m [DEVICE] " + device.name + " (type " + device.type + ") \x1b[0m")
            // genero un valore casuale di consumo per il device
            let consumption = getConsumptionByValue(device.type);
            try {
                // conteggio numero consumi
                let numberOfConsumptions = 0;
                new Promise((resolve, reject) => {
                    resolve(prisma.logger.findMany({ where: { deviceId: device.id, name: "consumption" }}));
                }).then((consumptions) => {
                    numberOfConsumptions = consumptions.length;
                    var valoreDaInserire = ((device.status === "active") ? (Math.random() * (consumption.max - consumption.min) + consumption.min).toFixed(2) : 0.0);
                    // inserimento nel DB
                    new Promise((resolve, reject) => {
                        resolve(prisma.logger.create({ 
                            data: { 
                                // device Device? @relation(fields: [deviceId], references: [id], onDelete: Cascade)
                                // deviceId String? @db.ObjectId
                                type: device.type,
                                name: "consumption",
                                device: {
                                    connect: {
                                        id: device.id
                                    }
                                },
                                value: parseFloat(valoreDaInserire)
                            }}));
                    }).then((dataInsertion) => {
                        if(dataInsertion){
                            console.info("[DEVICE] registered Random Consumption for Device '" + device.name + "' (type " + device.type + "): " + valoreDaInserire + " " + consumption.unit);
                            try{
                                for(let client of SOCKETS) {
                                    if(client.devices.includes(device.id)){
                                        let payload = JSON.stringify({ type: "REGISTERED_CONSUMPTION", device: device.id, value: valoreDaInserire, noConsumption: numberOfConsumptions });
                                        console.log("\x1b[95m [WEBSOCKET] Send REGISTERED_CONSUMPTION \x1b[0m")
                                        console.log("\x1b[95m [WEBSOCKET] " + payload + " \x1b[0m")
                                        if(client.ws.send(payload))
                                            console.log("INVIO " + JSON.stringify(payload))
                                    }
                                }
                            } catch (e) {
                                console.error("Exception 6: ")
                                console.log(e)
                            }

                            // triggerare ad "on" se tutte le condizioni sono state raggiunte per questo dispositivo, in almeno un'attività. Altrimenti "off".
                            // ispeziono le condizioni
                            new Promise((resolve, reject) => {
                                resolve(countSatisfiedConditions(device.actorType, device.id, device.status));
                            }).then(result => {
                                let newValue = result ? 'active' : 'inactive';
                                if(device.status !== newValue){
                                    new Promise((resolve, reject) => {
                                        resolve(prisma.device.update({
                                            where: { id: device.id },
                                            data: { 
                                                status: newValue
                                            }
                                        }));
                                    }).then((dataInsertion) => {
                                        if(dataInsertion){
                                            console.info("[DEVICE] updating the Device '" + device.name + "' (type " + device.type + ")'s status to " + (result ? 'ACTIVE' : 'INACTIVE') + ".");
                                            try {
                                                for(let client of SOCKETS) {
                                                    if(client.devices.includes(device.id)){
                                                        let payload = JSON.stringify({ type: "STATUS_UPDATE", device: device.id, status: newValue });
                                                        console.log("\x1b[95m [WEBSOCKET] Send STATUS_UPDATE \x1b[0m")
                                                        console.log("\x1b[95m [WEBSOCKET] " + payload + " \x1b[0m")
                                                        client.ws.send(payload)
                                                    }
                                                }
                                            } catch (e) {
                                                console.error("Exception 5: ")
                                                console.log(e)
                                            }
                                        }
                                        else console.error("Error while updating Device Status")
                                    }).catch((err) => { console.error("Exception 3: "); console.log(err); })  
                                } else {
                                    // console.log("\tDevice '" + device.name + "' (type " + device.type + ")'s status is already " + (result ? 'ACTIVE' : 'INACTIVE') + ".");
                                }    
                            }).catch(error => {
                                console.error('Errore durante il conteggio delle condizioni soddisfatte:', error);
                            });
                        }
                        else console.error("Error while inserting the random consumption into DB")
                    }).catch((err) => { console.error("Exception 1: "); console.log(err); })
                }).catch((err) => { console.error("Exception 0: "); console.log(err) })
            } catch (err) {
                console.error("Exception 2: ");
                console.log(err);
            }
        }
    }).catch((err) => { console.error("Exception 4: "); console.log(err); })
}, SECONDS * 1000);


/* Se il device è un SENSOR, posso loggare in value (usando MQTT del neslab.it, https://sensor.community/it/)
 * - temperatura
 * - umidità
 * - P1
 * - P2
 */
const mqtt = require('mqtt')
const protocol = 'mqtt'
const host = 'mqtt.neslab.it'
const port = '3200'
const connectUrl = `${protocol}://${host}:${port}`
const topic = '/smartcity/milan'

const client = mqtt.connect(connectUrl, {
    clean: true,
    connectTimeout: 4000,
    reconnectPeriod: 1000,
});


client.on('connect', () => {
    console.log('\x1b[33m [SENSORS] Connected to ' + connectUrl + '\x1b[0m')
    client.subscribe([topic], () => {
        console.log(`\x1b[33m [SENSORS] Subscribed to topic '${topic}' to get temperature, humidity and PM data of Milan.\x1b[0m`)
    })
});
client.on('error', (err) => {
    console.error('\x1b[33m[SENSORS] Error connecting to MQTT broker:' + err + '\x1b[0m');
});
client.on('message', (topic, payload) => {
    const data = JSON.parse(payload.toString());

    // per ora sto inserendo soltanto humidity e temperature, si può mettere anche pm25, pm10
    sensorTypes = ["humidity", "temperature"]

    if(
        data.hasOwnProperty("fields") /*&& 
        (data.fields.hasOwnProperty("humidity") || data.fields.hasOwnProperty("temperature"))*/
    ){
        Object.keys(data.fields).forEach(type => {
            if(sensorTypes.includes(type)){
                const value = data.fields[type];
                console.log(`\x1b[33m [SENSOR] Received Message(s) from '${topic}: ${type}: ${value} \x1b[0m`)
                
                // inserimento nel DB
                try {
                    // fetch all sensors in db
                    new Promise((resolve, reject) => {
                        resolve(prisma.device.findMany({ where: { type: { in : sensorTypes }, status: "active"}}));
                    }).then((devices) => {
                        for (let device of devices){
                            if(device.type === type){
                                new Promise((resolve, reject) => {
                                    resolve(prisma.logger.aggregate({ where: { deviceId: device.id }, _count: { value: true }}));
                                }).then((numberOfMeasurements) => {
                                    numberOfMeasurements = numberOfMeasurements._count.value;
                                    new Promise((resolve, reject) => {
                                        resolve(prisma.logger.create({ 
                                            data: { 
                                                // timestamp DateTime @id @default(now())
                                                // type String
                                                // name String?
                                                // device Device? @relation(fields: [deviceId], references: [id])
                                                // deviceId String? @db.ObjectId
                                                // value Float @default(0.00)
                                                type: type,
                                                name: "sensor",
                                                value: parseFloat(value),
                                                device: {
                                                    connect: {
                                                        id: device.id
                                                    }
                                                }
                                            }}));
                                    }).then((sensorInsertion) => {
                                        if(sensorInsertion){
                                            console.log("\x1b[33m [SENSOR] Sensor succesfully inserted into DB \x1b[0m");
                                            try{
                                                for(let client of SOCKETS) {
                                                    if(client.devices.includes(device.id)){
                                                        let payload = JSON.stringify({ type: "REGISTERED_CONSUMPTION", device: device.id, value: value, noConsumption: numberOfMeasurements });
                                                        console.log("\x1b[95m [WEBSOCKET] Send REGISTERED_CONSUMPTION \x1b[0m")
                                                        console.log("\x1b[95m [WEBSOCKET] " + payload + " \x1b[0m")
                                                        if(client.ws.send(payload))
                                                            console.log("INVIO " + JSON.stringify(payload))
                                                    }
                                                }
                                            } catch (e) {
                                                console.error("Exception 6: ")
                                                console.log(e)
                                            }
                                        } else console.error("Error while inserting into DB")
                                    }).catch((err) => { console.error("Exception while inserting into DB: "); console.log(err); })    
                                }).catch((err) => { console.error("Exception while inserting into DB: "); console.log(err); })
                            }
                        }
                    }).catch((err) => { console.error("Exception while inserting into DB: "); console.log(err); })   
                } catch (err) {
                    console.error("Exception while inserting into DB: ");
                    console.log(err)
                }
            }
        })
    }
})