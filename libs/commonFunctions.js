require("dotenv").config();
const jwt = require("jsonwebtoken");

function validateEmail(email) {
  // Regular expression pattern for email validation
  const emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

  // Check if the email matches the pattern
  return emailPattern.test(email);
}

function getUserDataByRequest(request){
  return jwt.verify(request.headers.authorization.split(" ")[1], process.env.TOKEN_KEY);
}

module.exports = { validateEmail, getUserDataByRequest };