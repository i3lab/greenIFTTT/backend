const { default: OpenAI } = require('openai');
require("dotenv").config();

const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();


const {getUserDataByRequest} = require("../libs/commonFunctions");
const { FsReadStream } = require('openai/_shims/auto/types');

const jsonConsumptionModel = {
        "date": "2021-03-10",
        "morning": 0.9977430555555555,
        "afternoon": 1.0032465277777778,
        "night": 0.9992510485320552
    }


const jsonModel =
{
    "type": "TYPE",
    "body": {
      "message": "Hello, world!",
      "timestamp": "2022-01-01T12:00:00Z",
      "routines": [
        {
          "name": "ROUTINE_NAME",
          "status": "ROUTINE_STATUS",
          "switchonoff": "ROUTINE_SWITCHONOFF",
          "description": "ROUTINE_DESCRIPTION",
          "activities": [
            {
              "name": "ACTIVITY_NAME",
              "status": "ACTIVITY_STATUS",
              "description": "ACTIVITY_DESCRIPTION",
              "conditions": [
                {
                    "name": "CONDITION_NAME",
                    "actorType": "CONDITION_ACTOR_TYPE",
                    "actorId": "DEVICE_ID",
                    "operator": "CONDITION_OPERATOR",
                    "value1": "CONDITION_VALUE1",
                    "value2": "CONDITION_VALUE2"
                }
              ],
              "nextDeviceStatus": "DEVICE_STATUS",
              "andConditions": true,
              "deviceId": "DEVICE_ID",
              "device": {
                "name": "DEVICE_NAME",
                "status": "DEVICE_STATUS",
                "type": "DEVICE_TYPE",
                "value": "DEVICE_VALUE",
                "startTimestamp": "2022-01-01T12:00:00Z",
                "endTimestamp": "2022-01-01T12:00:00Z"
              }
            }
          ]
        }
      ]
    }
}

const openai = new OpenAI({
    apiKey: process.env.OPENAI_KEY,
});


module.exports.chatGPTCall = async function chatGPTCall(req, res) {
    
    const userId = getUserDataByRequest(req).userId;
    const userInput = req.body.message;
    //console.log("USER INPUT: " + userInput);
    //----------------------------------------
    const routines = await prisma.routine.findMany({where: {userId: userId}});
    const routineIds = routines.map(routine => routine.id);
    const routineNames = routines.map(routine => routine.name);
    const activities = await prisma.activity.findMany({where: {routineId: {in: routineIds}}});
    const devices = await prisma.device.findMany({where: {userId: userId}});
    //console.log(routines);
    //console.log(activities);
    //console.log(devices);

    try {
        const addUserInput = new Promise((resolve, reject) => {
            resolve(prisma.chat.create({ 
                data: { 
                        userId: getUserDataByRequest(req).userId,
                        message: userInput,
                        timestamp: new Date(),
                        author: 'user'
                    }
                }));
        }).then((chat) => {
            if(!chat){
                res.status(500).send({ message: "Chat message not created"});
            } 
        }).catch((err) => { 
            console.error(err);
            return res.status(500).send({ message: "Internal server error: " + err }); 
        })
    } catch (err) {
        console.log(err);
        return res.status(500).send({ message: "Internal server error"});
    }

    //----------------------------------------NEW

    const threadByUser = {};

    if(!threadByUser[userId]){
        const mythread = await openai.beta.threads.create();
        threadByUser[userId] = mythread.id;
    }

    console.log("THREAD ID: " + threadByUser[userId]);

    try{
        const mythreadmessage = await openai.beta.threads.messages.create(
            threadByUser[userId],
            {
                role: "user",
                content: userInput,
            }
        );
        const assistantKey = "asst_JoJciQIaDgF8URozrOcD0MJi";
        const myrun = await openai.beta.threads.runs.create(

            threadByUser[userId],
            {
                assistant_id: assistantKey,
                instructions:`
                    You are a helpful assistant and should answer me clearly.
                    You have to always respond me only in a JSON format like the following: ${JSON.stringify(jsonModel)} and nothing else, You can never answer me in a different format, all your answers must be a json like the one showed before.
                    You cannot insert any text outside the JSON format, otherwise I will not understand you.
                    You have to ALWAYS answer me ONLY in the JSON format specified above, you cannot answer me with only pure text; 
                    even if you encounter some error, you have to report them in the JSON format, with type Chat.
                    If you don't know how to respond me, you can ask me to repeat the question (In the JSON format with type Chat) or to show you the available commands.
                    The message that you have to send me is in the 'message' field of the json.
                    Use ${JSON.stringify(devices)} to gather information about devices.

                    'TYPE' is 'Chat' if there is only text to show to user and 'routines' is null,
                    'TYPE' is 'Chat' if I ask you to create a routine but you don't find that device in: ${JSON.stringify(devices)}, so you will reply me with error message and 'routines' is null, otherwise the 'TYPE' is 'Routine' and 'routines' is filled in.  
                    'TYPE' is 'Routine' if there is some routine or activity going on (to be created or updated or deleted) and 'routines' list is filled in.
                    If I don't specify any ROUTINE_NAME, you put the activity inside the routine DAILY by default, if DAILY not exists create it.
                    If I specify also the ROUTINE_NAME or I put some specific words (vacation, trip, daily ecc), first you check if the routine already exists using this file: ${JSON.stringify(routines)}, 
                    if already exist, put the activity insite it,
                    otherwise create a new routine (with the name as general as possible) and put the activity inside the routine with that name.
                    If I ask you to turn on or off a routine, change the 'ROUTINE_STATUS' of the routine setting it to 'inactive' and set 'ROUTINE_SWITCHONOFF' to 'True' and
                    fill the 'activity' array list with all the activity in that routine (you have to use ${JSON.stringify(routines)} and ${JSON.stringify(activities)} to gathering informations) and set the 'ACTIVITY_STATUS' to 'inactive'.

                    'message' in the JSON is filled with a message to show to the user, it must be uman-like, friendly, in a way that I see you as my best friend.

                    In Routines list, 
                    'ROUTINE_NAME' can be whatever you want and
                    'ROUTINE_STATUS' can be 'Active' or 'Inactive'.
                    'ROUTINE_SWITCHONOFF' can be 'True' or 'False'. It is 'True' only if I ask you to turn on or off the routine, otherwise is 'False'.
                    'DEVICE_NAME' can be any kind of electronic appliance, sensor, etc. inside a house and 
                    'DEVICE_TYPE' can be 'Washing Machine', 'Dishwasher', 'Oven', 'Fridge', 'TV', 'PC', 'Lamp', 'Heater', 'Air Conditioner', 'Sensor', 'Other' in lowercase and if a device already exists, you can't modify.

                    Activities list is filled in only if there is some routine going on and 
                    'ACTIVITY_NAME' can be 'Postpone Washing Machine', 'Turn on Washing Machine', 'Turn off Washing Machine', 'Pause Washing Machine', 'Resume Washing Machine', 'Cancel Washing Machine' or other things about house appliances inside a house and 'ACTIVITY_STATUS' can be 'Active' or 'Inactive'.
                    'DEVICE_ID' is the id of the device to use and 'DEVICE_STATUS' can be 'Active' or 'Inactive'.
                    'device' is filled with the information from DEVICE_ID.

                    Conditions list is filled in only if there is some activity going on, but when some activity is going on the conditions list must be filled, and
                    'CONDITION_NAME' can be 'Time', 'Timestamp', 'Luminosity', 'Temperature', 'Humidity' or other things about sensors' data inside the house.
                    "CONDITION_ACTOR_TYPE" can be "sensor" or "timestamp".
                    "DEVICE_ID" is the id of the device to use that is related to the condition and you can find here: ${JSON.stringify(devices)}.
                    "CONDITION_OPERATOR" can be ">", "<", ">=", "<=", "=", "!=", "between", "not between".
                    "CONDITION_VALUE1" is the value to compare with and "CONDITION_VALUE2" is the value to compare with if "CONDITION_OPERATOR" is "between" or "not between".
                    When you have to use "CONDITION_VALUE1" and "CONDITION_VALUE2" you have set the "CONDITION_OPERATOR" to "between" or "not between".
                    "The condition you create should be when the device is powered on. If an object needs to be turned off,
                    you must create a complementary condition for when it needs to be turned on."

                    You have to gather information from the loaded files about the average energy consumption of the devices
                    during different time periods of a day, to respond me properly.
                    The files about consumption have json objects formatted like this: ${JSON.stringify(jsonConsumptionModel)} where 
                    the values are in kWh. If you don't know how to respond me or you don't find a certain devices in the files you were given,
                    check informations about the energy consumption on the internet.
                    If you are not able to access files, search informations on the internet and answer me in the JSON format with type chat.
                    If you encounter some problem in reading files or estracting some information, you can ask me to repeat the question
                    (In the JSON format with type Chat, without adding any pure text before or after the json format).
                    `, 
                
                tools: [
                    /*
                    You can, if I ask you, gather information from the loaded files about the average energy consumption of the devices
                    during different time periods of a day, to respond me properly.

                    I have uploaded you some file where you can fine average energy consumption about devices.
                    
                    */
                    { type: "code_interpreter"},
                    { type: "retrieval"},
                ],
            }
        );
        console.log("RUN OBJECT --------------------------------------------\n: " + myrun);  

        
        const retrieveRun = async () => {
            let keepretrivievingRun;

            while(myrun.status !== "completed"){
                keepretrivievingRun = await openai.beta.threads.runs.retrieve(threadByUser[userId], myrun.id);
                console.log("RUN STATUS: " + keepretrivievingRun.status);

                if(keepretrivievingRun.status === "completed"){
                    console.log("RUN STATUS: " + keepretrivievingRun.status);
                    break;
                }
            }
        }
        retrieveRun();

        const waitForAssistantMessage = async () => {
            await retrieveRun();

            const allMessages = await openai.beta.threads.messages.list(threadByUser[userId]);

            //console.log(allMessages);
            //console.log(allMessages.data[0].content);

            //console.log('BEFORE');
            //console.log(allMessages.data[0].content[0].text.value);

            //se continua a dare errori usare replaceAll()
            while(allMessages.data[0].content[0].text.value.includes('json')) {
                allMessages.data[0].content[0].text.value = allMessages.data[0].content[0].text.value.replace('json', '');
            }
            while( allMessages.data[0].content[0].text.value.includes('`')){
                allMessages.data[0].content[0].text.value = allMessages.data[0].content[0].text.value.replace('`', '');
            }

            //console.log('LATER');
            console.log(allMessages.data[0].content[0].text.value);

            let response;

            try {
                response = JSON.parse(allMessages.data[0].content[0].text.value);
                console.log(response);
                if (response.type == "Chat") {

                    const add = new Promise((resolve, reject) => {
                        console.log("USER ID: " + getUserDataByRequest(req).userId);
                        console.log("MESSAGE: " + response.body.message);
                        resolve(prisma.chat.create({ 
                            data: { 
                                    userId: getUserDataByRequest(req).userId,
                                    message: response.body.message,
                                    timestamp: new Date(),
                                    author: 'chatbot'
                                }
                            }));
                    }).then((chat) => {
                        if(!chat){
                            return res.status(500).send({ message: "Chat message not created"});
                        } else {
                            console.log(chat);
                            return res.status(200).send(chat);
                        }
                    }).catch((err) => { 
                        console.error(err);
                        //return res.status(500).send({ message: "Internal server error: " + err }); 
                    })
                }
                else if (response.type == "Routine") {
                    correct = false;
    
                    while(!correct){
                    correct = true;
                    try {
                        /*if(response.message == "Internal server error") {
                            chatGPTCall(req, res);
                        }*/
    
                        //add message to database
                        const add = new Promise((resolve, reject) => {
                            console.log("USER ID: " + getUserDataByRequest(req).userId);
                            console.log("MESSAGE: " + response.body.message);
                            resolve(prisma.chat.create({ 
                                data: { 
                                        userId: getUserDataByRequest(req).userId,
                                        message: response.body.message,
                                        timestamp: new Date(),
                                        author: 'chatbot'
                                    }
                                }));
                        }).then((chat) => {
                            if(!chat){
                                return res.status(500).send({ message: "Chat message not created"});
                            } else {
                                return res.status(200).send(chat);
                            }
                        }).catch((err) => { 
                            console.error(err);
                            correct = false;
                            //return res.status(500).send({ message: "Internal server error: " + err }); 
                        })
    
                        // add routine to database
                        const user = getUserDataByRequest(req);
                        const userDevices = new Promise((resolve, reject) => {
                            resolve(prisma.device.findMany({where: {userId: user.id}}));
                        }).then((devices) => {
                            const userDeviceNames = devices.map(device => device.name);
                        }).catch((err) => { 
                            //return res.status(500).send({ message: "Internal server error: " + JSON.stringify(err)}); 
                            correct = false;
                        })
    
                        console.log(response.body.routines[0]);
                        console.log(response.body.routines[0].activities[0]);
                        const activityKeys = Object.keys(response.body.routines[0].activities[0]);
                        const routineKeys = Object.keys(response.body.routines[0]);
    
                        for (const activity in response.body.routines[0].activities) {
                            if (!activityKeys.includes("name") || !activityKeys.includes("status") || !activityKeys.includes("description") || !activityKeys.includes("nextDeviceStatus") || !activityKeys.includes("andConditions") || !activityKeys.includes("deviceId")) {
                                return res.status(500).send({ message: "Activity not created"});
                            }
                            else if (activity.name === null || activity.status === null || activity.description === null || activity.nextDeviceStatus === null || activity.andConditions === null || activity.deviceId === null) {
                                return res.status(500).send({ message: "Activity contains null values"});
                            }
                        }
    
                        for (const routine in response.body.routines) {
                            if (!routineKeys.includes("name") || !routineKeys.includes("status") || !routineKeys.includes("description")) {
                                return res.status(500).send({ message: "Routine not created"});
                            }
                            else if (routine.name === null || routine.status === null || routine.description === null) {
                                return res.status(500).send({ message: "Routine contains null values"});
                            }
                        }
    
                        if(routineNames.includes(response.body.routines[0].name)) {
                            if (response.body.routines[0].switchonoff == "True") {
                                const updateRoutinePromise = new Promise((resolve, reject) => {
                                    //console.log("BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB")
                                    resolve(prisma.routine.update({
                                        where: { id: routines[routineNames.indexOf(response.body.routines[0].name)].id },
                                        data: {
                                            status: response.body.routines[0].status.toLowerCase(), 
                                        }
                                    }));
                                }).then((activity) => {
                                    if(!activity){
                                        return res.status(500).send({ message: "Not updated"});
                                    } else {
                                        return res.status(200).send(activity);
                                    }
                                }).catch((err) => {
                                    //return res.status(500).send({ message: "Internal server error: " + JSON.stringify(err)});
                                    correct = false;
                                })
                            }else if(response.body.routines[0].switchonoff == "False"){
                                //console.log("WE YOU ARE IN");
                                const addActivityPromise = new Promise((resolve, reject) => {
                                    //console.log("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
                                    resolve(prisma.routine.update({
                                        where: { id: routines[routineNames.indexOf(response.body.routines[0].name)].id },
                                        data: {
                                            activities: {
                                                create: response.body.routines[0].activities.map(activity => ({
                                                    name: activity.name,
                                                    status: activity.status.toLowerCase(),
                                                    description: activity.description,
                                                    nextDeviceStatus: activity.nextDeviceStatus.toLowerCase(),
                                                    andConditions: true,
                                                    device: {
                                                        connect: {
                                                            id: activity.deviceId,
                                                        }
                                                    },
                                                    conditions: {
                                                        create: activity.conditions.map(condition => ({
                                                            name: condition.name,
                                                            actorType: condition.actorType,
                                                            actorId: condition.actorId,
                                                            operator: condition.operator,
                                                            value1: condition.value1,
                                                            value2: condition.value2
                                                        }))
                                                    }
                                                }))
                                            }
                                        }
                                    }));
                                }).then((activity) => {
                                    if(!activity){
                                        return res.status(500).send({ message: "Activity not created"});
                                    } else {
                                        return res.status(200).send(activity);
                                    }
                                }).catch((err) => { 
                                    //return res.status(500).send({ message: "Internal server error: " + JSON.stringify(err)}); 
                                    correct = false;
                                })
                                }
                        } else{
                            const addRoutinePromise = new Promise((resolve, reject) => {
                                resolve(prisma.routine.create({ 
                                    data: { 
                                            userId: getUserDataByRequest(req).userId, // to dynamically get the userId from the call
                                            name: response.body.routines[0].name, 
                                            status: response.body.routines[0].status.toLowerCase(),
                                            description: response.body.routines[0].description,
                                            activities: {
                                                create: response.body.routines[0].activities.map(activity => ({
                                                    name: activity.name,
                                                    status: activity.status.toLowerCase(),
                                                    description: activity.description,
                                                    nextDeviceStatus: activity.nextDeviceStatus.toLowerCase(),
                                                    andConditions: true,
                                                    device: {
                                                        connect: {
                                                            id: activity.deviceId,
                                                        }
                                                    },
                                                    conditions: {
                                                        create: activity.conditions.map(condition => ({
                                                            name: condition.name,
                                                            actorType: condition.actorType,
                                                            actorId: condition.actorId,
                                                            operator: condition.operator,
                                                            value1: condition.value1,
                                                            value2: condition.value2
                                                        }))
                                                    }
                                                }))
                                            }
                                        }}));
                            }).then((routine) => {
                                if(!routine){
                                    return res.status(500).send({ message: "Routine non created"});
                                } else {
                                    return res.status(200).send(routine);
                                }
                            }).catch((err) => { 
                                correct = false;
                                //return res.status(500).send({ message: "Internal server error: " + JSON.stringify(err)}); 
                            })                
                        }        
                    } catch (err) {
                        console.log(err);
                        correct = false;
                        //return res.status(500).send({ message: "Internal server error"});
                    }
                }
                
                }
            } catch (error) {

                console.error("Error parsing JSON:", error);
                const add = new Promise((resolve, reject) => {
                    console.log("USER ID: " + getUserDataByRequest(req).userId);
                    console.log("MESSAGE: " + response);
                    resolve(prisma.chat.create({ 
                        data: { 
                                userId: getUserDataByRequest(req).userId,
                                message: 'There has been an error in the response, please try again',
                                timestamp: new Date(),
                                author: 'chatbot'
                            }
                        }));
                }).then((chat) => {
                    if(!chat){
                        return res.status(500).send({ message: "Chat message not created"});
                    } else {
                        console.log(chat);
                        return res.status(200).send(chat);
                    }
                }).catch((err) => { 
                    console.error(err);
                    //return res.status(500).send({ message: "Internal server error: " + err }); 
                })

                
            }
            
            
            
        } 
        waitForAssistantMessage();
    }catch(err){
        console.log("ERROR -----------------------------------------\n"+err);
    }
    
}

module.exports.getChatHistory = function getChatHistory(req, res) {
    try {
        const getChatHistoryPromise = new Promise((resolve, reject) => {
            resolve(
                prisma.chat.findMany({
                    where: { userId: getUserDataByRequest(req).userId },
                    take: 10, // Limit the result to 10 messages
                    orderBy: { timestamp: 'desc' }, // Order the messages by timestamp in descending order
                })
            );
        }).then((chatHistory) => {
            if(!chatHistory){
                return res.status(500).send({ message: "Chat history not found"});
            } else {
                const reversedChatHistory = chatHistory.reverse();
                return res.status(200).send(reversedChatHistory);
            }
        }).catch((err) => { return res.status(500).send({ message: "Internal server error: " + JSON.stringify(err)}); })        
    } catch (err) {
        console.log(err);
        return res.status(500).send({ message: "Internal server error"});
    }
}

module.exports.deleteChatHistory = function deleteChatHistory(req, res) {
    try {
        const deleteChatHistoryPromise = new Promise((resolve, reject) => {
            resolve(
                prisma.chat.deleteMany({
                    where: { userId: getUserDataByRequest(req).userId },
                })
            );
        }).then((chatHistory) => {
            if(!chatHistory){
                return res.status(500).send({ message: "Chat history not deleted"});
            } else {
                return res.status(200).send({ message: "Chat history deleted"});
            }
        }).catch((err) => { return res.status(500).send({ message: "Internal server error: " + JSON.stringify(err)}); })        
    } catch (err) {
        console.log(err);
        return res.status(500).send({ message: "Internal server error"});
    }
}