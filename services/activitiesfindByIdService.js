const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();
const { getUserDataByRequest } = require("../libs/commonFunctions");

module.exports.getActivityById = function getActivityById(req, res) {
    try{
        const getActivitiesByIdPromise = new Promise((resolve, reject) => {
            resolve(prisma.activity.findUnique({where: { id: parseInt(req.params.id)}}));
        }).then((activities) => {
            // check if activities exist
            if(activities.length <= 0){
                return res.status(400).send({ message: "No activities found"});
            } else {
                res.status(200).send(activities);
            }
        }).catch((err) => { return res.status(500).send({ message: "Internal server error: " + JSON.stringify(err)}); })
    }catch(err){
        console.log(err);
        return res.status(500).send({ message: "Internal server error"});
    }
}

