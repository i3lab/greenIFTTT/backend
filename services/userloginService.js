const { validateEmail } = require("../libs/commonFunctions");
require("dotenv").config();
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();

module.exports.loginUser = function loginUser(req, res) {
    try {
        // get user input
        const { email, password } = req.body;

        // validate user input
        if (!(validateEmail(email) && password != "")) {
            res.status(405).send({ message: "Invalid input"});
        }

        // check if user already exist
        const findUserPromise = new Promise((resolve, reject) => {
            resolve(prisma.user.findFirst({ where: { email: email } }));
        }).then((user) => {
            if(user){
                const validatePwdPromise = new Promise((resolve, reject) => {
                    resolve(bcrypt.compare(password, user.password));
                }).then((comparisonResult) => {
                    if(comparisonResult){
                        // create token
                        const token = jwt.sign({ 
                            userId: user.id,
                            name: user.name,
                            surname: user.surname,
                            email: user.email 
                        }, process.env.TOKEN_KEY, {
                            expiresIn: "2h",
                        });
                
                        // save user token
                        user.token = token;
                        // remove user password
                        delete user.password;
                        
                        // return new user
                        res.status(200).send(user);
                    } else {
                        return res.status(404).send({ message: "Wrong credentials."});
                    }
                }).catch((err) => { return res.status(500).send({ message: "Internal server error" }); })
            } else {
                return res.status(404).send({ message: "User not found or wrong email/password" });
            }
        }).catch((err) => { return res.status(500).send({ message: "Internal server error" }); })        
    } catch (err) {
        return res.status(500).send({ message: "Internal server error"});
    }
}

