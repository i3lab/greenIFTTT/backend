const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();
const { getUserDataByRequest } = require("../libs/commonFunctions");

module.exports.getActivitiesByRoutine = function getActivitiesByRoutine(req, res) {
    try{
        const getActivitiesByRoutinePromise = new Promise((resolve, reject) => {
            resolve(prisma.activity.findMany({ include: { device: true }, where: { routineId: req.query.routineId }, orderBy: { id: 'desc' }}));
        }).then((activities) => {
            // check if activities exist
            if(activities.length <= 0){
                return res.status(400).send({ message: "No activities found"});
            } else {
                res.status(200).send(activities);
            }
        }).catch((err) => { return res.status(500).send({ message: "Internal server error: " + JSON.stringify(err)}); })
    }catch(err){
        console.log(err);
        return res.status(500).send({ message: "Internal server error"});
    }
}

