const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();
const {getUserDataByRequest} = require("../libs/commonFunctions");
const DEVICE_TYPES = require("../../frontend/src/assets/deviceTypes.json");

module.exports.getDevices = async function getDevices(req, res) {
    try {
        const oggi = new Date(); // oggi
        const unaSettimanaFa = new Date(oggi.getTime() - 7 * 24 * 60 * 60 * 1000); // a week ago

        let devices = await prisma.device.findMany({ where: { userId: getUserDataByRequest(req).userId }, orderBy: { id: 'desc' }});
        // check if devices exist
        if(!devices || devices.length <= 0){
            return res.status(400).send({ message: "No devices found in the whole house."});
        } else {
            for(let i = 0; i < devices.length; i++){
                // prendo ultimo consumo e consumo medio
                let lastConsumption = 0;
                let avgConsumption = 0;
                let consumptionUnit = "Wh";

                // default parameters
                devices[i].value = lastConsumption + " " + consumptionUnit
                devices[i].weekConsumptions = avgConsumption + " " + consumptionUnit

                // take last measurement
                const lC = await prisma.logger.findFirst({
                    where: {
                        deviceId: devices[i].id,
                    },
                    orderBy: {
                        timestamp: 'desc',
                    }
                });

                if(lC){
                    lastConsumption = (lC.value) ? (lC.value.toFixed(2)) : 0.0;

                    // take average consumption
                    const avgC = await prisma.logger.aggregate({
                        where: {
                            deviceId: devices[i].id,
                            timestamp: {
                                gte: unaSettimanaFa,
                            },
                        },
                        _avg: {
                            value: true,
                        }
                    });
                    if(avgC){
                        avgConsumption = (avgC._avg.value) ? (avgC._avg.value).toFixed(2) : 0.0;
                    }

                    // controllo il tipo del dispositivo
                    const deviceTypeItem = DEVICE_TYPES.flatMap(item => item.items).find(item => item.value === devices[i].type);
                    // TODO: nel caso di sensori che cambiano, sistemare qui!!!!
                    let unit = "Wh";
                    if (!["temperature", "humidity"].includes(devices[i].type)){
                        // dispositivo normale...
                        unit = deviceTypeItem.consumption.unit ? deviceTypeItem.consumption.unit : "Wh";
                    } else {
                        // sensore...
                        unit = deviceTypeItem.unityOfMeasure ? deviceTypeItem.unityOfMeasure : "Wh";
                    }
                    devices[i].weekConsumptions = avgConsumption + " " + unit
                    devices[i].value = lastConsumption + " " + unit;
                }
            }
            return res.status(200).send(devices);
        }
    } catch (err) {
        console.log(err)
        return res.status(500).send({ message: "Internal server error: " + JSON.stringify(err)});
    }
}

module.exports.updateDevice = function updateDevice(req, res) {
    try {
        const { id, name, type, status, room, icon } = req.body;

        // update device in database
        const updateDevicePromise = new Promise((resolve, reject) => {
            resolve(prisma.device.update({ 
                where: { id: id, userId: getUserDataByRequest(req).userId }, 
                data: { 
                        name: name, 
                        type: type, 
                        status: status,
                        room: room,
                        icon: icon
                    }}));
        }).then((device) => {
            if(!device){
                return res.status(400).send({ message: "Error while updating the device" });
            } else {
                return res.status(200).send(device);
            }
        }).catch((err) => { return res.status(500).send({ message: "Internal server error: " + JSON.stringify(err)}); })        
    } catch (err) {
        return res.status(500).send({ message: "Internal server error"});
    }
}

module.exports.addDevice = async function addDevice(req, res) {
    try {
        const { name, type, status, room, icon } = req.body;

         //Check if device already exists
         const devices = await prisma.device.findMany({ where: { userId: getUserDataByRequest(req).userId }});
         const devicesNames = devices.map(device => device.name);
         if(devicesNames.includes(name)){
             return res.status(400).send({ message: "Device name already exists"});
         }

        // add device to database
        const dettagli = { 
            userId: getUserDataByRequest(req).userId,
            name: name, 
            type: type, 
            // status: status,
            room: room,
            icon: icon
        };
        const addDevicePromise = new Promise((resolve, reject) => {
            resolve(prisma.device.create({ 
                data: dettagli}));
        }).then((device) => {
            if(!device){
                return res.status(400).send({ message: "Error while adding the device"});
            } else {
                return res.status(200).send(device);
            }
        }).catch((err) => { return res.status(500).send({ message: "Internal server error: " + JSON.stringify(err)}); })        
    } catch (err) {
        console.log(err);
        return res.status(500).send({ message: "Internal server error"});
    }
}


module.exports.deleteDevice = function deleteDevice(req, res) {
    try {
        const { id } = req.body;
        // delete device from database
        const deleteDevicePromise = new Promise((resolve, reject) => {
            resolve(prisma.device.delete({ 
                where: { id: id, userId: getUserDataByRequest(req).userId }}));
        }).then((device) => {
            if(!device){
                return res.status(400).send({ message: "Error while removing the device" });
            } else {
                return res.status(200).send({ message: "Device deleted" });
            }
        }).catch((err) => { return res.status(500).send({ message: "Internal server error: " + JSON.stringify(err)}); })        
    } catch (err) {
        console.log(err);
        return res.status(500).send({ message: "Internal server error"});
    }
}

