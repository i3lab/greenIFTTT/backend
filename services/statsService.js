const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();
const { getUserDataByRequest } = require("../libs/commonFunctions");

module.exports.getDevicesStats = function getDevicesStats(req, res) {
    try {
        const user = getUserDataByRequest(req);
        
        const dateFrom = req.query.from;
        const dateTo = req.query.to;

        let where = { userId: user.userId };

        new Promise((resolve, reject) => {
            resolve(prisma.device.findMany({ where: where, orderBy: { name: 'asc' }}));
        }).then(async (devices) => {
            // check if devices exist
            if (devices.length <= 0) {
                return res.status(400).send({ message: "No devices found" });
            } else {
                if(dateFrom || dateTo){
                    where.timestamp = {}
                    if(dateFrom) where.timestamp.gte = new Date(dateFrom);
                    if(dateTo) where.timestamp.lte = new Date(dateTo);
                }
                delete where.userId;

                // for each device, find all datas
                for(let device of devices){
                    where.deviceId = device.id 
                    device.logs = await prisma.logger.findMany({ select: { timestamp: true, name: true, type: true, value: true }, where: where, orderBy: { timestamp: 'asc' }});
                }

                return res.status(200).send(devices);
            }
        }).catch((err) => { return res.status(500).send({ message: "Internal server error: " + JSON.stringify(err) }); })
    } catch (err) {
        console.log(err);
        return res.status(500).send({ message: err });
    }
}