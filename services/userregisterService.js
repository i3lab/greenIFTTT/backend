const { validateEmail } = require("../libs/commonFunctions");
require("dotenv").config();
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();

module.exports.registerUser = function registerUser(req, res) {
    try {
        // get user input
        const { name, surname, email, password } = req.body;

        // validate user input
        if (!(name != "" && surname != "" && validateEmail(email) && password != "")) {
            res.status(405).send({ message: "Invalid input"});
        }

        // check if user already exist
        const findOldUserPromise = new Promise((resolve, reject) => {
            resolve(prisma.user.findFirst({ where: { email: email } }));
        }).then((oldUser) => {
            if(oldUser){
                return res.status(405).send({ message: "User Already Exist. Please Login" });
            }

            // encrypt user password
            const encryptedPasswordPromise = new Promise((resolve, reject) => {
                resolve(bcrypt.hash(password, 10));
            }).then ((encryptedPassword) => {
                // Create user in our database
                const createUserPromise = new Promise((resolve, reject) => {
                    resolve(prisma.user.create({ 
                        data: {
                            name: name,
                            surname: surname,
                            email: email,
                            password: encryptedPassword,
                        }
                    }));
                }).then((user) => {
                    // create token
                    const token = jwt.sign({ 
                        userId: user.id,
                        name: user.name,
                        surname: user.surname,
                        email: user.email
                    }, process.env.TOKEN_KEY, {
                        expiresIn: "2h",
                    });
            
                    // save user token
                    user.token = token;
                    // remove user password
                    delete user.password;
                    // return new user
                    res.status(200).send(user);
                }).catch((err) => { return res.status(500).send({ message: "Internal server error: " + JSON.stringify(err)}); })
            }).catch((err) => { return res.status(500).send({ message: "Internal server error: " + JSON.stringify(err)}); })
        }).catch((err) => { return res.status(500).send({ message: "Internal server error: " + JSON.stringify(err)}); })        
    } catch (err) {
        return res.status(500).send({ message: "Internal server error"});
    }
}