const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();
const { getUserDataByRequest } = require("../libs/commonFunctions");

module.exports.getDevicesByName = function getDevicesByName(req, res) {
    try{
        const getDevicesByNamePromise = new Promise((resolve, reject) => {
            resolve(prisma.device.findMany({where: { name: req.params.name}}));
        }).then((devices) => {
            // check if devices exist
            if(devices.length <= 0){
                return res.status(400).send({ message: "No devices found"});
            } else {
                res.status(200).send(devices);
            }
        }).catch((err) => { return res.status(500).send({ message: "Internal server error: " + JSON.stringify(err)}); })
    }catch(err){
        console.log(err);
        return res.status(500).send({ message: "Internal server error"});
    }
}

