const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();
const {getUserDataByRequest} = require("../libs/commonFunctions");
const DEVICE_TYPES = require("../../frontend/src/assets/deviceTypes.json");
const ROOMS = require("../../frontend/src/assets/rooms.json");

const oggi = new Date(); // Oggi
const unaSettimanaFa = new Date(oggi.getTime() - 7 * 24 * 60 * 60 * 1000); // Una settimana fa

module.exports.getDevicesByRoom = function getDevicesByRoom(req, res) {
    try{
        const getDevicesByRoomPromise = new Promise((resolve, reject) => {
            resolve(prisma.device.findMany({where: { room: req.query.room, userId: getUserDataByRequest(req).userId }, orderBy: { id: 'desc' }}));
        }).then((devices) => {
            // check if devices exist
            if(!devices || devices.length <= 0){
                res.status(400).send({ message: "No devices found in the " + ROOMS.find(r => r.value === req.query.room).text + "."});
            } else {
                for(let i = 0; i < devices.length; i++){
                    // prendo ultimo consumo e consumo medio
                    let lastConsumption = 0;
                    let avgConsumption = 0;

                    new Promise((resolve, reject) => { resolve(prisma.logger.findFirst({
                        where: {
                            deviceId: devices[i].id,
                        },
                        orderBy: {
                            timestamp: 'desc',
                        },
                    }))}).then((lC) => {
                        if(lC){
                            lastConsumption = lC.value.toFixed(2);
                        }

                        new Promise((resolve, reject) => { resolve(prisma.logger.aggregate({
                            where: {
                                deviceId: devices[i].id,
                                timestamp: {
                                    gte: unaSettimanaFa,
                                },
                            },
                            _avg: {
                                value: true,
                            },
                        }))}).then((avgC) => {
                            if(avgC){
                                avgConsumption = (avgC._avg.value) ? (avgC._avg.value).toFixed(2) : 0.0;
                            }

                            // controllo il tipo del dispositivo
                            const deviceTypeItem = DEVICE_TYPES.flatMap(item => item.items).find(item => item.value === devices[i].type);
                            // TODO: nel caso di sensori che cambiano, sistemare qui!!!!
                            if (!["temperature", "humidity", "P1", "P2"].includes(deviceTypeItem)){
                                devices[i].weekConsumptions = avgConsumption + " " + deviceTypeItem.consumption.unit
                                devices[i].value = lastConsumption + " " + deviceTypeItem.consumption.unit
                            } else {
                                devices[i].value = lastConsumption + " " + deviceTypeItem.unityOfMeasure;
                            }

                            return res.status(200).send(devices);
                        })
                    });
                }
            }
        }).catch((err) => { console.log(err); res.status(500).send({ message: "Internal server error: " + JSON.stringify(err)}); })
    }catch(err){
        console.log(err);
        return res.status(500).send({ message: "Internal server error"});
    }
}

