const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();
const { getUserDataByRequest } = require("../libs/commonFunctions");
const { chatgpt } = require("./chatService");

module.exports.getAdvices = function getAdvices(req, res) {
    try {
        const user = getUserDataByRequest(req);
        const getAdvices = new Promise((resolve, reject) => {
            resolve(prisma.advice.findMany({ where: { userId: user.id }, orderBy: { id: 'desc' } }));
        }).then((advices) => {
            // check if advices exist
            if (advices.length <= 0) {
                return res.status(400).send({ message: "No advices found" });
            } else {
                res.status(200).send(advices);
            }
        }).catch((err) => { return res.status(500).send({ message: "Internal server error: " + JSON.stringify(err) }); })
    } catch (err) {
        console.log(err);
        return res.status(500).send({ message: "Internal server error" });
    }
}


const { default: OpenAI } = require('openai');
const openai = new OpenAI({
    apiKey: process.env.OPENAI_KEY,
});

module.exports.addAdvice = async function addAdvice(req, res) {
    try {  
        const userId = getUserDataByRequest(req).userId;
        const devices = await prisma.device.findMany({where: {userId: userId}});
        // Call chatgpt and store the returned value in advice
        const threadByUser = {};

        if(!threadByUser[userId]){
            const mythread = await openai.beta.threads.create();
            threadByUser[userId] = mythread.id;
        }

        console.log("THREAD ID: " + threadByUser[userId]);

        try{
            const mythreadmessage = await openai.beta.threads.messages.create(
                threadByUser[userId],
                {
                    role: "user",
                    content: "I need some advice about my house, in general",
                }
            );
            const assistantKey = "asst_JoJciQIaDgF8URozrOcD0MJi";
            const myrun = await openai.beta.threads.runs.create(

                threadByUser[userId],
                {
                    assistant_id: assistantKey,
                    instructions:`
                        You are a helpful assistant and should answer me clearly, conisely and shortly.
                        At most 60 world.
                        You have to always respond me only in a json format like the following: ${JSON.stringify(jsonModel)} and nothing else.
                        The message that you have to send me is in the 'message' field of the json.
                        Use ${JSON.stringify(devices)} to gather information about devices.

                        I need small tips about my house.
                        I need hint on how to save energy, save money, and how to make my house more efficient.
                        Use the file that I have uploaded you, and information on the internet to answer me.

                        You can't answer me with questions.
                        You can't answer me with a list.
                        You can't answer me with error messages.
                        `, 
                    
                    tools: [
                        /*
                        You can, if I ask you, gather information from the loaded files about the average energy consumption of the devices
                        during different time periods of a day, to respond me properly.

                        I have uploaded you some file where you can fine average energy consumption about devices.
                        
                        */
                        { type: "code_interpreter"},
                        { type: "retrieval"},
                    ],
                }
            );
            console.log("RUN OBJECT --------------------------------------------\n: " + myrun);  

            
            const retrieveRun = async () => {
                let keepretrivievingRun;

                while(myrun.status !== "completed"){
                    keepretrivievingRun = await openai.beta.threads.runs.retrieve(threadByUser[userId], myrun.id);
                    console.log("RUN STATUS: " + keepretrivievingRun.status);

                    if(keepretrivievingRun.status === "completed"){
                        console.log("RUN STATUS: " + keepretrivievingRun.status);
                        break;
                    }
                }
            }
            retrieveRun();

            const waitForAssistantMessage = async () => {
                await retrieveRun();

                const allMessages = await openai.beta.threads.messages.list(threadByUser[userId]);

                //console.log(allMessages);
                //console.log(allMessages.data[0].content);

                //console.log('BEFORE');
                //console.log(allMessages.data[0].content[0].text.value);

                //se continua a dare errori usare replaceAll()
                while(allMessages.data[0].content[0].text.value.includes('json')) {
                    allMessages.data[0].content[0].text.value = allMessages.data[0].content[0].text.value.replace('json', '');
                }
                while( allMessages.data[0].content[0].text.value.includes('`')){
                    allMessages.data[0].content[0].text.value = allMessages.data[0].content[0].text.value.replace('`', '');
                }

                //console.log('LATER');
                //console.log(allMessages.data[0].content[0].text.value);

                const response = JSON.parse(allMessages.data[0].content[0].text.value);
                
                console.log(response);

                //.........................
                
                 // add advice to database
                const addAdvicePromise = new Promise((resolve, reject) => {
                    resolve(prisma.advice.create(
                        { data: 
                            { 
                                userId: getUserDataByRequest(req).userId, 
                                message:response.body.message, 
                                timestamp:new Date() 
                            } 
                        }));
                }).then((advice) => {
                    if (!advice) {
                        return res.status(400).send({ message: "Invalid advice" });
                    } else {
                        return res.status(200).send(advice);
                    }
                }).catch((err) => { return res.status(500).send({ message: "Internal server error: " + err }); })
                
            }
            waitForAssistantMessage();


        }catch(err){
            console.log(err);
            return res.status(500).send({ message: "Internal server error" });
        }
    } catch (err) {
        console.log(err);
        return res.status(500).send({ message: "Internal server error" });
    }
}

const jsonModel =
{
    "type": "TYPE",
    "body": {
      "message": "Hello, world!",
      "timestamp": "2022-01-01T12:00:00Z",
      "routines": [
        {
          "name": "ROUTINE_NAME",
          "status": "ROUTINE_STATUS",
          "description": "ROUTINE_DESCRIPTION",
          "activities": [
            {
              "name": "ACTIVITY_NAME",
              "status": "ACTIVITY_STATUS",
              "description": "ACTIVITY_DESCRIPTION",
              "conditions": [
                {
                    "name": "CONDITION_NAME",
                    "actorType": "CONDITION_ACTOR_TYPE",
                    "actorId": "DEVICE_ID",
                    "operator": "CONDITION_OPERATOR",
                    "value1": "CONDITION_VALUE1",
                    "value2": "CONDITION_VALUE2"
                }
              ],
              "nextDeviceStatus": "DEVICE_STATUS",
              "andConditions": true,
              "deviceId": "DEVICE_ID",
              "device": {
                "name": "DEVICE_NAME",
                "status": "DEVICE_STATUS",
                "type": "DEVICE_TYPE",
                "value": "DEVICE_VALUE",
                "startTimestamp": "2022-01-01T12:00:00Z",
                "endTimestamp": "2022-01-01T12:00:00Z"
              }
            }
          ]
        }
      ]
    }
}