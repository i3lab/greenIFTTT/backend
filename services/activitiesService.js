const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();
const {getUserDataByRequest} = require("../libs/commonFunctions");

module.exports.getActivities = function getActivities(req, res) {
    // TODO: è buggato... ritorna tutte le activities di tutti gli utenti!
    try{
        const user = getUserDataByRequest(req);
        const getRoutines = new Promise((resolve, reject) => {
            resolve(prisma.routine.findMany({ where: { userId: user.id }, orderBy: { id: 'desc' }}));
        }).then((routines) => {
            const routineIds = routines.map(routine => routine.id);
        
            const getActivities = new Promise((resolve, reject) => {
                resolve(prisma.activity.findMany({ include: { device: true }, where: {routineId: {in: routineIds}}}));
            }).then((activities) => {
                // check if activities exist
                if(activities.length <= 0){
                    return res.status(400).send({ message: "No activities found"});
                } else {
                    res.status(200).send(activities);
                }
            }).catch((err) => { return res.status(500).send({ message: "Internal server error: " + JSON.stringify(err)}); })
        }).catch((err) => { return res.status(500).send({ message: "Internal server error: " + JSON.stringify(err)}); })
    } catch(err){
        console.log(err);
        return res.status(500).send({ message: "Internal server error"});
    }

}


module.exports.updateActivity = function updateActivity(req, res) {
    try {
        const { id, name, status, description, deviceId, conditions, andConditions} = req.body;
        console.log(req.body);
        // update activity in database
        const updateActivityPromise = new Promise((resolve, reject) => {
            resolve(prisma.activity.update({ 
                where: { id: id }, 
                data: { 
                        name: name,
                        status: status,
                        description: description,
                        deviceId: deviceId,
                        conditions: conditions,
                        andConditions: andConditions
                    }}));
        }).then((activity) => {
            if(!activity){
                return res.status(400).send({ message: "Invalid activity"});
            } else {
                return res.status(200).send(activity);
            }
        }).catch((err) => { return res.status(500).send({ message: "Internal server error: " + JSON.stringify(err)}); })        
    } catch (err) {
        console.log(err);
        return res.status(500).send({ message: "Internal server error"});
    }
}

module.exports.addActivity = function addActivity(req, res) {
    try {
        const { name, status, description, conditions, nextDeviceStatus, andConditions, deviceId, routineId } = req.body;

        console.log(req.body);
        console.log(getUserDataByRequest(req));
        // add activity to database
        const addActivityPromise = new Promise((resolve, reject) => {
            resolve(prisma.activity.create({ 
                data: { 
                        name: name,
                        status: status,
                        description: description,
                        conditions: conditions,
                        andConditions: andConditions,
                        nextDeviceStatus: nextDeviceStatus,
                        deviceId: deviceId,
                        routineId: routineId
                    }}));
        }).then((activity) => {
            if(!activity){
                return res.status(400).send({ message: "Invalid activity"});
            } else {
                return res.status(200).send(activity);
            }
        }).catch((err) => { return res.status(500).send({ message: "Internal server error: " + JSON.stringify(err)}); })        
    } catch (err) {
        console.log(err);
        return res.status(500).send({ message: "Internal server error" + JSON.stringify(err)});
    }
}

module.exports.deleteActivity = function deleteActivity(req, res) {
    try {
        const { id } = req.body;

        // delete activity from database
        const deleteActivityPromise = new Promise((resolve, reject) => {
            resolve(prisma.activity.delete({ 
                where: { id: id }}));
        }).then((activity) => {
            if(!activity){
                return res.status(400).send({ message: "Invalid activity" });
            } else {
                return res.status(200).send({ message: "Activity deleted" });
            }
        }).catch((err) => { return res.status(500).send({ message: "Internal server error: " + JSON.stringify(err)}); })        
    } catch (err) {
        console.log(err);
        return res.status(500).send({ message: "Internal server error"});
    }
}

