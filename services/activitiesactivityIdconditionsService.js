const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();
const { getUserDataByRequest } = require("../libs/commonFunctions");

module.exports.getConditions = function getConditions(req, res) {
    try{
        const getConditionsPromise = new Promise((resolve, reject) => {
            console.log("ACTIVITY ID: " + req.params.activityId);
            resolve(prisma.condition.findMany({where: {activityId: req.params.activityId}, orderBy: { id: 'desc' }} ));
            
        }).then((conditions) => {
            // check if conditions exist
            if(conditions.length <= 0){
                return res.status(400).send({ message: "No conditions found"});
            } else {
                res.status(200).send(conditions);
            }
        }).catch((err) => { return res.status(500).send({ message: "Internal server error: " + err}); })
    }catch(err){
        console.log(err);
        return res.status(500).send({ message: "Internal server error"});
    }    
}

module.exports.addCondition = function addCondition(req, res) {
    try {
        const { name, actorType, actorId, operator, value1, value2, activityId } = req.body;

        // add condition to database
        const addConditionPromise = new Promise((resolve, reject) => {
            resolve(prisma.condition.create({ 
                data: { 
                        name: name,
                        actorType: actorType,
                        actorId: actorId,
                        operator: operator,
                        value1: value1,
                        value2: value2,
                        activityId: activityId
                    }}));
        }).then((condition) => {
            if(!condition){
                return res.status(400).send({ message: "Invalid condition"});
            } else {
                return res.status(200).send(condition);
            }
        }).catch((err) => { return res.status(500).send({ message: "Internal server error: " + JSON.stringify(err)}); })        
    } catch (err) {
        console.log(err);
        return res.status(500).send({ message: "Internal server error"});
    }
}

module.exports.updateCondition = function updateCondition(req, res) {
    try {
        const { id, name, actorType, actorId, operator, value1, value2, activityId } = req.body;

        // update condition in database
        const updateConditionPromise = new Promise((resolve, reject) => {
            resolve(prisma.condition.update({ 
                where: { id: id }, 
                data: { 
                        name: name,
                        actorType: actorType,
                        actorId: actorId,
                        operator: operator,
                        value1: value1,
                        value2: value2,
                        activityId: activityId
                    }}));
        }).then((condition) => {
            if(!condition){
                return res.status(400).send({ message: "Invalid condition"});
            } else {
                return res.status(200).send(condition);
            }
        }).catch((err) => { return res.status(500).send({ message: "Internal server error: " + JSON.stringify(err)}); })        
    } catch (err) {
        console.log(err);
        return res.status(500).send({ message: "Internal server error"});
    }
}

module.exports.deleteCondition = function deleteCondition(req, res) {
    try {
        const { id } = req.body;

        // delete condition from database
        const deleteConditionPromise = new Promise((resolve, reject) => {
            resolve(prisma.condition.delete({ 
                where: { id: id }}));
        }).then((condition) => {
            if(!condition){
                return res.status(400).send({ message: "Invalid condition"});
            } else {
                return res.status(200).send(condition);
            }
        }).catch((err) => { return res.status(500).send({ message: "Internal server error: " + JSON.stringify(err)}); })        
    } catch (err) {
        console.log(err);
        return res.status(500).send({ message: "Internal server error"});
    }  
}

