const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();
const {getUserDataByRequest} = require("../libs/commonFunctions");

module.exports.getRoutines = function getRoutines(req, res) {
    try {
        const getAllRoutinesPromise = new Promise((resolve, reject) => {
            resolve(prisma.routine.findMany({ where: { userId: getUserDataByRequest(req).userId }, orderBy: { id: 'desc' }}));
        }).then((routines) => {
            // check if routines exist
            if(routines.length <= 0){
                return res.status(404).send({ message: "No routines found"});
            } else {
                console.log(routines);
                return res.status(200).send( routines );
            }
        }).catch((err) => { return res.status(500).send({ message: "Internal server error: " + JSON.stringify(err)}); })        
    } catch (err) {
        console.log(err);
        return res.status(500).json({ message: "Internal server error"});
    }
}

module.exports.updateRoutine = function updateRoutine(req, res) {
    try {
        const { id, name, status, description, activities} = req.body;

        console.log(req.body);
        // update routine in database
        const updateRoutinePromise = new Promise((resolve, reject) => {
            resolve(prisma.routine.update({ 
                where: { id: id, userId: getUserDataByRequest(req).userId }, 
                data: { 
                        name: name, 
                        status: status,
                        description: description,
                        activities: activities
                    }}));
        }).then((routine) => {
            if(!routine){
                return res.status(400).send({ message: "Invalid routine"});
            } else {
                return res.status(200).send(routine);
            }
        }).catch((err) => { return res.status(500).send({ message: "Internal server error: " + JSON.stringify(err)}); })        
    } catch (err) {
        console.log(err);
        return res.status(500).send({ message: "Internal server error"});
    }
}

module.exports.addRoutine = function addRoutine(req, res) {
    try {
        const { name, status, description, activities } = req.body;

        // add routine to database
        const addRoutinePromise = new Promise((resolve, reject) => {
            resolve(prisma.routine.create({ 
                data: { 
                        userId: getUserDataByRequest(req).userId, // to dynamically get the userId from the call
                        name: name, 
                        status: status,
                        description: description,
                        activities: activities
                    }}));
        }).then((routine) => {
            if(!routine){
                return res.status(400).send({ message: "Invalid routine"});
            } else {
                return res.status(200).send(routine);
            }
        }).catch((err) => { return res.status(500).send({ message: "Internal server error: " + JSON.stringify(err)}); })        
    } catch (err) {
        console.log(err);
        return res.status(500).send({ message: "Internal server error"});
    }
}

module.exports.deleteRoutine = function deleteRoutine(req, res) {
    try {
        const { id } = req.body;
        console.log(id);
        console.log(req.body);
        // delete routine from database
        const deleteRoutinePromise = new Promise((resolve, reject) => {
            resolve(prisma.routine.delete({ 
                where: { id: id, userId: getUserDataByRequest(req).userId }}));
        }).then((routine) => {
            if(!routine){
                return res.status(400).send({ message: "Invalid routine" });
            } else {
                return res.status(200).send({ message: "Routine deleted" });
            }
        }).catch((err) => { return res.status(500).send({ message: "Internal server error1: " + JSON.stringify(err)}); })        
    } catch (err) {
        console.log(err);
        return res.status(500).send({ message: "Internal server error2: " + JSON.stringify(err)});
    }
}

