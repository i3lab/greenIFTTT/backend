const { getUserDataByRequest } = require("../libs/commonFunctions");

module.exports.getUser = function getUser(req, res) {
    userData = getUserDataByRequest(req);
    res.send(userData);
}

